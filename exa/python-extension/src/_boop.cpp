#include<pybind11/pybind11.h>
#include<pybind11/stl.h>
#include<string>

std::string beep(){
  return "beep";
}

PYBIND11_MODULE(_boop,m){
  m.def("beep",&beep);
}
